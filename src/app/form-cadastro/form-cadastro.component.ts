import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-form-cadastro',
  templateUrl: './form-cadastro.component.html',
  styleUrls: ['./form-cadastro.component.scss']
})
export class FormCadastroComponent implements OnInit {

  formExib: any;
  logoTeam: any;

  equipes = [
    {
      nomeEquipe: 'BCA',
      urlImg: ''
    },
    {
      nomeEquipe: 'DCAP',
      urlImg: ''
    },
    {
      nomeEquipe: 'DELTA',
      urlImg: ''
    },
    {
      nomeEquipe: 'GRA',
      urlImg: ''
    },
    {
      nomeEquipe: 'IMPACTO',
      urlImg: ''
    },
    {
      nomeEquipe: 'MAT',
      urlImg: 'assets/img/logo-center-login.png'
    },
    {
      nomeEquipe: 'SAS',
      urlImg: ''
    },
    {
      nomeEquipe: 'SEALS',
      urlImg: ''
    },
    {
      nomeEquipe: 'SPEATNAZ',
      urlImg: ''
    }
  ];

  funcaoOperador = [
    {
      nmFuncao: 'ASSAULT'
    },
    {
      nmFuncao: 'DMR'
    },
    {
      nmFuncao: 'SNIPER'
    },
    {
      nmFuncao: 'SUPORTE'
    }
]

  constructor() { }

  ngOnInit(): void {
    this.formExib = false;
    this.logoTeam = "";
  }

  isExibirForm() {
    return this.formExib
  }

  exibirBotao(){
    this.formExib = this.formExib ? false : true;
  }

  exibirLogo() {
    return this.logoTeam;
  }

  mudarLogo(url: any) {
    this.logoTeam = url.split(" - ")[1];
  }

}
